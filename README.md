# Computer Aquarium - HackNC 2019

This repo contains code for creating your own "computer aquarium".
"Fish" in this aquarium are actually VMs on Google Cloud. They
move around the tank in response to sending and receiving network traffic.

![Architecture](architecture.png)

## VM Setup

1. Log into Google Cloud Platform
2. Go to https://console.cloud.google.com/networking/networks/list
3. Click "Create a VPC Network". This will be the aquarium. Ensure you add a subnet and enable "Flow logs" in that subnet"
4. Add two firewall rules to the VPC to allow port 22 (for ssh) and port 8000 on all instances
5. Go to https://console.cloud.google.com/compute/instanceGroups/list
6. Create a group of 8 VMs all running Centos 7
7. View the network logs [here](https://console.cloud.google.com/logs/viewer?resource=gce_subnetwork&project=hacknc-2019&minLogLevel=0&expandAll=false&timestamp=2019-10-12T15:47:14.080000000Z&customFacets=&limitCustomFacetWidth=true&advancedFilter=logName:(projects%2Fhacknc-2019%2Flogs%2Fcompute.googleapis.com%252Fvpc_flows)%20AND%20resource.labels.subnetwork_id:(4001500600998843398)&dateRangeStart=2019-10-12T14:47:14.345Z&dateRangeEnd=2019-10-12T15:47:14.345Z&interval=PT1H)

## Demo Setup

1. Run the ansible-playbookover every fish to start a webserver and a script that randomly curls other fish
    ```bash
    ansible-playbook fish.yml
    ```

2. Set up the server vm
    ```bash
    sudo yum install python3
    ```

3. Run the ansible-playbook to deploy and run the main server
    ```bash
    ansible-playbook server.yml
    ```
