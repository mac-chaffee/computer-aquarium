import os
import asyncio
import json
from typing import Optional
from http import HTTPStatus
import websockets
from google.cloud import pubsub
from google.cloud.pubsub_v1.types import FlowControl
from gcloud.aio.pubsub import SubscriberClient


class WebSocketServerProtocolWithHTTP(websockets.WebSocketServerProtocol):
    """Implements a simple static file server for WebSocketServer.
    Taken from https://gist.github.com/artizirk/04eb23d957d7916c01ca632bb27d5436
    """

    async def process_request(self, path, request_headers):
        """Serves index.html and matter.js only"""

        if "Upgrade" in request_headers:
            print("UPGRADE")
            return  # Probably a WebSocket connection

        response_headers = [
            ('Server', 'asyncio'),
            ('Connection', 'close'),
        ]

        if path == '/':
            path = 'index.html'
            response_headers.append(('Content-Type', 'text/html'))
        elif path == '/matter.min.js':
            path = 'matter.min.js'
            response_headers.append(('Content-Type', 'text/javascript'))
        else:
            return HTTPStatus.NOT_FOUND, [], b'404 NOT FOUND'

        with open(path, 'rb') as f:
            body = f.read()
            response_headers.append(('Content-Length', str(len(body))))
            return HTTPStatus.OK, response_headers, body


class CustomSubscriberClient(SubscriberClient):
    """Custom aio client so I can init it using from_service_account_json"""
    def __init__(self, *, loop=None) -> None:
        self._subscriber = pubsub.SubscriberClient.from_service_account_json('hacknc-test-key.json')
        self.loop = loop or asyncio.get_event_loop()


async def main(websocket, path):
    # Define a callback here so it has access to the websocket variable
    async def callback(message):
        data = json.loads(message.data)['jsonPayload']
        packets_sent = int(data['packets_sent'])
        vm1 = None
        vm2 = None
        if 'src_instance' in data:
            vm1 = data['src_instance']['vm_name']
        if 'dest_instance' in data:
            vm2 = data['dest_instance']['vm_name']

        await websocket.send(json.dumps({"from": vm1, "to": vm2, "num": packets_sent}))
        message.ack()

    client = CustomSubscriberClient()
    flow_control = FlowControl(max_messages=100)
    client.subscribe('projects/hacknc-2019/subscriptions/aquarium-server', callback=callback, flow_control=flow_control)

    # Let the client run forever
    while True:
        await asyncio.sleep(60)



if __name__ == '__main__':
    start_server = websockets.serve(main, None, 8000, create_protocol=WebSocketServerProtocolWithHTTP)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
